import Mongoose, {Schema, Model, Types} from 'mongoose';
interface tas{
    name: String;
    created_date: Date;
    status: String

}

const TaskSchema: Schema = new Schema<any>({
    name: {
        type: String,
        required: 'Enter the name of the task'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    status: {
        type: [{
            type: String,
            enum: ['pending', 'ongoing', 'completed']
        }],
        default: ['pending']
    }
});

const TaskModel: Model<tas> = Mongoose.model('Task', TaskSchema);

export default TaskModel;
