import { Request, Response} from 'express';
import Mongoose, {Model} from 'mongoose';
import TaskModel from '../Models/todoListModel';

export class todoListController {
    constructor() {
    }

    public listAllTasks = (req: Request, res: Response) => {
        TaskModel.find({}, (err, task) => {
            if (err) res.send(err); // Or throw new Error(err.message);
            return res.json(task); // Or return task;
        })
    }

    public createTask = (req: Request, res: Response) => {
        const NEW_TASK = new TaskModel(req?.body);
        NEW_TASK?.save( (err, task) => {
            if (err) res.send(err);
            return res.json(task);
        })
    }

    public readTask = (req: Request, res: Response) => {
        TaskModel?.findById(req?.params?.taskId, (err: any, task: any) => {
            if (err) res.send(err);
            res.json(task);
        })
    }

    public updateTask = (req: Request, res: Response) => {
        TaskModel?.findOneAndUpdate(
            {
                _id: req.params.taskId
            },
            req?.body,
            {
                new: true
            },
            (err, task) => {
                if (err) res.send(err);
                res.json(task);
            }
        )
    }

    public deleteTask = (req: Request, res: Response) => {
        TaskModel?.deleteOne({ _id: req?.params?.taskId }, (err) => {
            if (err) res.send(err);
            res.json({ message: 'Task successfully deleted'});
        });
    }
}
export default new todoListController()
