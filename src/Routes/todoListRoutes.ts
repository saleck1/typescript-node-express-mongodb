import todoList from '../controllers/todoListController';
class Route{

    public paths = (app: any) => {
        app.route('/tasks')
            .get(todoList?.listAllTasks)
            .post(todoList?.createTask);

        app.route('/tasks/:taskId')
            .get(todoList?.readTask)
            .put(todoList?.updateTask)
            .delete(todoList?.deleteTask)
    }
}


export default new Route();
