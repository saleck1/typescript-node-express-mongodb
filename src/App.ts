import express, {Application} from 'express';
import bodyParser from 'body-parser';
import Mongoose from 'mongoose';
import TaskRoute from './Routes/todoListRoutes';
import {ConnectionOptions} from "tls";

export class App {
    public app: Application = express();
    private readonly PORT: string | number = process.env.PORT || 3000;

    constructor() {
        this.initialize();
        this.DBInit();
        this.configuationRoutes();
        this.initializeServer();
    }

    private initialize() {
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
    }

    private DBInit() {
        Mongoose.Promise = global.Promise;
        Mongoose.connect('mongodb://localhost/Tododb', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        } as ConnectionOptions).then(() => {
            console.log("Connected to Database");
        }).catch((err) => {
            console.log("Not Connected to Database ERROR! ", err);
        });
    }

    private configuationRoutes() {
        TaskRoute.paths(this.app);
    }

    private initializeServer() {
        this.app.listen( this.PORT, () => console.log('todo list api server listening at ' + this.PORT));
    }
}

new App();

